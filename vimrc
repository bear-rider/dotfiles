" Plugins will be downloaded under the specified directory.
call plug#begin('~/.vim/plugged')
" Declare the list of plugins.
" List ends here. Plugins become visible to Vim after this call.
call plug#end()

set nocompatible

filetype off
filetype plugin indent on

set ttyfast

syntax enable

" Show file options above the command line
set wildmenu
set wildignore+=node_modules/*

set laststatus=2
set encoding=utf-8
set autoread
set autoindent
set autochdir
set backspace=indent,eol,start
set incsearch
set hlsearch
set smartcase
set smartindent
set ignorecase


" for text column
set textwidth=90
set colorcolumn=+1
set nowrap           " do not automatically wrap on load
set formatoptions-=t " do not automatically wrap text when typing

" Basic vim settings
set hidden
set visualbell
set number relativenumber
set nobackup
set noswapfile
set noshowmode
set showmatch
set ruler

if &diff
  " colorscheme evening
  highlight DiffAdd    cterm=bold ctermfg=10 ctermbg=17 gui=none guifg=bg guibg=Red
  highlight DiffDelete cterm=bold ctermfg=10 ctermbg=17 gui=none guifg=bg guibg=Red
  highlight DiffChange cterm=bold ctermfg=10 ctermbg=17 gui=none guifg=bg guibg=Red
  highlight DiffText   cterm=bold ctermfg=10 ctermbg=88 gui=none guifg=bg guibg=Red
endif


" leader mappings
let mapleader="-"
let maplocalleader="\\"

" edit and load vimrc in normal mode
nnoremap <leader>ev :split $MYVIMRC<cr>
nnoremap <leader>sv :source $MYVIMRC<cr>

" save and go to normal with jk
inoremap jk <esc>:w<CR>

" save and quite with jkk
inoremap jkk <esc>:wq<CR>

" filetype specific abbreviations
source ~/.vimconfigs/cfnmappings.vimrc
source ~/.vimconfigs/pyfiles.vimrc
source ~/.vimconfigs/htmlfiles.vimrc

" surroung with quotes
nnoremap <leader>" viw<esc>a"<esc>bi"<esc>lela
nnoremap <leader>' viw<esc>a'<esc>bi'<esc>lela
nnoremap <leader>` viw<esc>a`<esc>bi`<esc>lela
nnoremap <leader>> viw<esc>a><esc>bi<<esc>lela
nnoremap <leader>/ viw<esc>a><esc>bi<\<esc>lela

" run prettier on buffer
nnoremap <leader>js <esc>:%!prettier . %

" run black on buffer
nnoremap <leader>py <esc>:%!black --quiet . %

" run isort on buffer
nnoremap <leader>is <esc>:%!isort --balanced --quiet --trailing-comma . %

" disable arrow keys in normal mode
nnoremap <left> <nop>
nnoremap <right> <nop>
nnoremap <up> <nop>
nnoremap <down> <nop>

" run jq on cfn to filter resources
nnoremap <leader>CR :%!jq '[.Resources[].Type] \| sort'

" braces for json files
nnoremap <leader>{ i<space>{<cr><space><space><cr>}<esc>klla
nnoremap <leader>[ i<space>[<cr><space><space><cr>]<esc>klla

" remove trailing while space
autocmd BufWritePre * %s/\s\+$//e

" Global tab width.
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab

" Set the terminal's title
set titlestring=%t%(\ %M%)%(\ (%{expand(\"%:p:h\")})%)%(\ %a%)\ -\ %{v:servername}

" Set to show invisibles (tabs & trailing spaces) & their highlight color
set list listchars=tab:�\ ,trail:�,extends:#,nbsp:.

" comment using local leader
autocmd FileType javascript nnoremap <buffer> <localleader>c I//<esc>

" complete tags
iabbrev >> <esc>hdi>pla</<esc>pa><esc>
